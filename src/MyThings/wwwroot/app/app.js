﻿(function () {
    "use strict";

    var app = angular.module("app", ["ngRoute", "wt.responsive", "socialLogin", "ngCookies"]);

    //app.run(["googleService", function (googleService) {
    //    googleService.initGapi();
    //}]);

    app.config(function (socialProvider) {
        socialProvider.setGoogleKey("424903973850-q6aa44c73lv9e05vcju2t9duq0n9spvt.apps.googleusercontent.com");
    });

    app.config(["$routeProvider",
        function ($routeProvider) {
            $routeProvider
                .when("/", {
                    templateUrl: "app/login/login.html",
                    controller: "loginController"
                })
                .when("/things", {
                    templateUrl: "app/things/things.html",
                    controller: "thingsController"
                })
                .when("/things/:id", {
                    templateUrl: "app/things/thing.html",
                    controller: "thingController"
                })
        .otherwise({
            redirectTo: "/"
        });
        }]);
})();