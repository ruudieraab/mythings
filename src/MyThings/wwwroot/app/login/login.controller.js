﻿(function () {
    "use strict";
    var controllerId = "loginController";
    angular.module("app").controller(controllerId, ["$scope", "$http", "$location", "$rootScope", "$cookies", loginController]);

    function loginController($scope, $http, $location, $rootScope, $cookies) {
        $scope.goToThings = goToThings;
        $scope.loggingIn = false;

        $rootScope.$on("event:social-sign-in-success", function (event, userDetails) {
            if (!$scope.loggingIn) {
                login(userDetails);
            }
        });

        function goToThings() {
            $location.path("/things");
        }

        function login(userDetails) {
            $scope.loggingIn = true;

            var user = {
                "network": "google",
                "networkuserid": userDetails.uid,
                "email": userDetails.email
            }
            console.log(user)
            $http.post("/api/users/", user, { header: { "Content-Type": "application/json" } })
                .then(function (response) {
                    $cookies.put("userid", response.data.id);
                    goToThings();
                },
                function (response) {
                    $scope.errorText = response.statusCode;
                }).finally(function () {
                    $scope.loggingIn = false;
                });
        }

        // googleService.initGapi();
    }
})();