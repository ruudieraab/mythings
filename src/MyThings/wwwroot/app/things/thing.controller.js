﻿(function () {
    "use strict";
    var controllerId = "thingController";
    angular.module("app").controller(controllerId, ["$scope", "$http", "$location", "$routeParams", thingController]);

    function thingController($scope, $http, $location, $routeParams) {
        var createNew = $routeParams.id === "new";
        $scope.goToThings = goToThings;
        $scope.remove = remove;
        $scope.save = save;
        $scope.errorText = "";

        $scope.isLoading = false;
        $scope.isSaving = false;

        initThing();
        createTitle();


        function initThing() {
            if (!createNew) {
                getThing($routeParams.id);
                return;
            }

            $scope.thing = {
                nameOf: null,
                description: null,
                purchasePrice: null,
                purchaseYear: null,
                purchaseMonth: null,
                purchaseDay: null,
                imageSrc: null,
                receiptSrc: null,
                giftFrom: null,
                tags: null,
                quantity: 1
            }
        }

        function goToThings() {
            $location.path("/things");
        }

        function createTitle() {
            if (createNew) {
                $scope.title = "ny pryl";
                return;
            }

            $scope.title = "ändra pryl";
        }

        function getThing(id) {
            $scope.isLoading = true;

            $http({
                url: "/api/things/" + id,
                method: "GET"
            }).then(function (response) {
                response.data.purchaseDate = new Date(response.data.purchaseDate);
                $scope.thing = response.data;
            }, function (response) {
                $scope.errorText = response.statusCode;
            }).finally(function () {
                $scope.isLoading = false;
            });
        }

        function save() {
            $scope.isSaving = true;

            if (createNew) {
                $http.post("/api/things/", $scope.thing, { header: { "Content-Type": "application/json" } })
                    .then(function (response) {
                        goToThings();
                    },
                        function (response) {
                            $scope.errorText = response.statusCode;
                        }).finally(function () {
                            $scope.isSaving = false;
                        });
            } else {
                $http.put("/api/things/" + $routeParams.id, $scope.thing, { header: { "Content-Type": "application/json" } })
                    .then(function (response) {
                        goToThings();
                    }, function (response) {
                        $scope.errorText = response.statusCode;
                    }).finally(function () {
                        $scope.isSaving = false;
                    });
            }
        }

        function remove() {
            $scope.isSaving = true;

            $http.delete("/api/things/" + $routeParams.id)
                .then(function (response) {
                    goToThings();
                },
                function (error) {
                    $scope.errorText = response.statusCode;
                }).finally(function () {
                    $scope.isSaving = false;
                });
        }
    }
})();