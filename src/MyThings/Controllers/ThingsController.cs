﻿namespace MyThings.Controllers
{
    using System;
    using Data;
    using Data.Models;
    using Microsoft.AspNetCore.Mvc;

    [Route("api/things")]
    public class ThingsController : Controller
    {
        [HttpGet]
        public IActionResult GetAll()
        {
            var httpContext = HttpContext;
            var context = httpContext.RequestServices.GetService(typeof(ThingContext)) as ThingContext;
            var things = context.GetAllThings();
            
            return new JsonResult(things);
        }

        [HttpGet("{id}")]
        public IActionResult GetOne(Guid id)
        {
            var httpContext = HttpContext;
            var context = httpContext.RequestServices.GetService(typeof(ThingContext)) as ThingContext;
            var things = context.GetOneThing(id);

            return new JsonResult(things);
        }

        [HttpPost]
        public IActionResult Create([FromBody]Thing thing)
        {
            var httpContext = HttpContext;
            var context = httpContext.RequestServices.GetService(typeof(ThingContext)) as ThingContext;

            var created = context.Create(thing);

            return new JsonResult(created);
        }

        [HttpPut("{id}")]
        public IActionResult Update(Guid id, [FromBody]Thing thing)
        {
            var httpContext = HttpContext;
            var context = httpContext.RequestServices.GetService(typeof(ThingContext)) as ThingContext;

            var created = context.Update(id, thing);

            return new JsonResult(created);
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(Guid id)
        {
            var httpContext = HttpContext;
            var context = httpContext.RequestServices.GetService(typeof(ThingContext)) as ThingContext;

            var created = context.Remove(id);

            return new JsonResult(created);
        }
    }
}