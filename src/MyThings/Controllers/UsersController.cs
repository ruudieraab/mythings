﻿namespace MyThings.Controllers
{
    using System;
    using Data;
    using Data.Models;
    using Microsoft.AspNetCore.Mvc;

    [Route("api/users")]
    public class UsersController : Controller
    {
        [HttpGet]
        public IActionResult GetAll()
        {
            var httpContext = HttpContext;
            var context = httpContext.RequestServices.GetService(typeof(ThingContext)) as ThingContext;
            var things = context.GetAllThings();
            
            return new JsonResult(things);
        }

        [HttpGet("{id}")]
        public IActionResult GetOne(Guid id)
        {
            var httpContext = HttpContext;
            var context = httpContext.RequestServices.GetService(typeof(ThingContext)) as ThingContext;
            var things = context.GetOneThing(id);

            return new JsonResult(things);
        }

        [HttpPost]
        public IActionResult Login([FromBody]User user)
        {
            var httpContext = HttpContext;
            var context = httpContext.RequestServices.GetService(typeof(UserContext)) as UserContext;

            var created = context.Create(user);

            return new JsonResult(created);
        }

        [HttpPut("{id}")]
        public IActionResult Update(Guid id, [FromBody]Thing thing)
        {
            var httpContext = HttpContext;
            var context = httpContext.RequestServices.GetService(typeof(ThingContext)) as ThingContext;

            var created = context.Update(id, thing);

            return new JsonResult(created);
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(Guid id)
        {
            var httpContext = HttpContext;
            var context = httpContext.RequestServices.GetService(typeof(ThingContext)) as ThingContext;

            var created = context.Remove(id);

            return new JsonResult(created);
        }
    }
}