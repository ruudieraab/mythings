﻿namespace MyThings.Data
{
    using System;
    using System.Collections.Generic;
    using Models;
    using MySql.Data.MySqlClient;

    public class UserContext
    {
        public string ConnectionString { get; set; }

        public UserContext(string connectionString)
        {
            this.ConnectionString = connectionString;
        }

        private MySqlConnection GetConnection()
        {
            return new MySqlConnection(ConnectionString);
        }

        public User GetUserByNetworkAndNetworkUserId(string network, string networkUserId)
        {
            using (var conn = GetConnection())
            {
                conn.Open();
                var cmd = new MySqlCommand("SELECT * FROM users where network = '" + network + "' and networkuserid = '" + networkUserId + "';", conn);
                using (var reader = cmd.ExecuteReader())
                {
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            return ConvertFromDbToUser(reader);
                        }
                    }
                    else
                    {
                        return null;
                    }
                }
            }

            throw new ArgumentException();
        }

        public User Create(User user)
        {
            var existingUser = this.GetUserByNetworkAndNetworkUserId(user.Network, user.NetworkUserId);

            if (existingUser != null)
            {
                return existingUser;
            }
            else
            {
                using (var conn = GetConnection())
                {
                    conn.Open();
                    var sql =
                        "INSERT INTO users (networkuserid, network, email)";
                    sql += " VALUES(@networkuserid, @network, @email)";

                    var cmd = new MySqlCommand(sql, conn);
                    AddParameters(cmd, user);
                    cmd.ExecuteNonQuery();

                    var newUser = this.GetUserByNetworkAndNetworkUserId(user.Network, user.NetworkUserId);

                    if (newUser != null)
                    {
                        return newUser;
                    }

                    throw new Exception("could not create user");
                }
            }
        }

        private static User ConvertFromDbToUser(MySqlDataReader reader)
        {
            var user = new User
            {
                Id = reader.GetGuid("id"),
                NetworkUserId = reader.IsDBNull(reader.GetOrdinal("networkuserid")) ? null : reader.GetString("networkuserid"),
                Network = reader.IsDBNull(reader.GetOrdinal("network")) ? null : reader.GetString("network"),
                Email = reader.IsDBNull(reader.GetOrdinal("email")) ? null : reader.GetString("email")
            };

            return user;
        }

        private static void AddParameters(MySqlCommand cmd, User user)
        {
            cmd.Parameters.AddWithValue("@networkuserid", user.NetworkUserId);
            cmd.Parameters.AddWithValue("@network", user.Network);
            cmd.Parameters.AddWithValue("@email", user.Email);
        }
    }
}