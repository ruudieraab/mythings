﻿namespace MyThings.Data.Models
{
    using System;

    public class Thing
    {
        public Guid? Id { get; set; }

        public string NameOf { get; set; }

        public string Description { get; set; }

        public int? PurchasePrice { get; set; }

        public int? PurchaseYear { get; set; }

        public int? PurchaseMonth { get; set; }

        public int? PurchaseDay { get; set; }

        public string ImageSrc { get; set; }

        public string ReceiptSrc { get; set; }

        public int? Quantity { get; set; }

        public string GiftFrom { get; set; }

        public string UserId { get; set; }

        public DateTime? PurchaseDate
        {
            get
            {
                if (PurchaseYear.HasValue && PurchaseMonth.HasValue && PurchaseDay.HasValue)
                {
                    return new DateTime(PurchaseYear.Value, PurchaseMonth.Value, PurchaseDay.Value);
                }

                return null;
            }
        }
    }
}