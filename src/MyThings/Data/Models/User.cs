﻿namespace MyThings.Data.Models
{
    using System;

    public class User
    {
        public Guid? Id { get; set; }

        public string NetworkUserId { get; set; }

        public string Network { get; set; }
        
        public string Email { get; set; }
    }
}