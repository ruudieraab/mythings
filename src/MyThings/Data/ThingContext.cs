﻿namespace MyThings.Data
{
    using System;
    using System.Collections.Generic;
    using Models;
    using MySql.Data.MySqlClient;

    public class ThingContext
    {
        public string ConnectionString { get; set; }

        public ThingContext(string connectionString)
        {
            this.ConnectionString = connectionString;
        }

        private MySqlConnection GetConnection()
        {
            return new MySqlConnection(ConnectionString);
        }

        public List<Thing> GetAllThings()
        {
            var things = new List<Thing>();

            using (var conn = GetConnection())
            {
                conn.Open();
                var cmd = new MySqlCommand("SELECT * FROM things", conn);
                using (var reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        things.Add(ConvertFromDbToThing(reader));
                    }
                }
            }

            return things;
        }

        public Thing GetOneThing(Guid id)
        {
            using (var conn = GetConnection())
            {
                conn.Open();
                var cmd = new MySqlCommand("SELECT * FROM things where id = '" + id + "'", conn);
                using (var reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        return ConvertFromDbToThing(reader);
                    }
                }
            }

            throw new ArgumentException();
        }

        public bool Create(Thing thing)
        {
            using (var conn = GetConnection())
            {
                conn.Open();
                var sql =
                    "INSERT INTO things (nameof, purchaseprice, quantity, description, purchaseyear, purchasemonth, purchaseday, imagesrc, receiptsrc, giftfrom)";
                sql += " VALUES(@nameof, @purchaseprice, @quantity, @description, @purchaseyear, @purchasemonth, @purchaseday, @imagesrc, @receiptsrc, @giftfrom)";

                var cmd = new MySqlCommand(sql, conn);
                AddParameters(cmd, thing);
                cmd.ExecuteNonQuery();
                return true;
            }
        }

        public Thing Update(Guid id, Thing thing)
        {
            using (var conn = GetConnection())
            {
                conn.Open();
                var sql = "UPDATE things";
                sql += " SET nameof=@nameof, purchaseprice=@purchaseprice, quantity=@quantity, description=@description, purchaseyear=@purchaseyear, purchasemonth=@purchasemonth";
                sql+= ", purchaseday=@purchaseday, imagesrc=@imagesrc, receiptsrc=@receiptsrc, giftfrom=@giftfrom";
                sql += " WHERE id = '" + id + "';";

                var cmd = new MySqlCommand(sql, conn);
                AddParameters(cmd, thing);
                cmd.ExecuteNonQuery();

                return GetOneThing(id);
            }
        }

        public bool Remove(Guid id)
        {
            using (var conn = GetConnection())
            {
                conn.Open();
                var sql = "delete from things";
                sql += " where id = '" + id + "';";

                var cmd = new MySqlCommand(sql, conn);
                cmd.ExecuteNonQuery();

                return true;
            }
        }

        private static Thing ConvertFromDbToThing(MySqlDataReader reader)
        {
            var thing = new Thing
            {
                Id = reader.GetGuid("id"),
                NameOf = reader.IsDBNull(reader.GetOrdinal("nameof")) ? null : reader.GetString("nameof"),
                Description = reader.IsDBNull(reader.GetOrdinal("description")) ? null : reader.GetString("description"),
                PurchasePrice = reader.IsDBNull(reader.GetOrdinal("purchaseprice")) ? (int?)null : reader.GetInt32("purchaseprice"),
                PurchaseYear = reader.IsDBNull(reader.GetOrdinal("purchaseyear")) ? (int?)null : reader.GetInt32("purchaseyear"),
                PurchaseMonth = reader.IsDBNull(reader.GetOrdinal("purchasemonth")) ? (int?)null : reader.GetInt32("purchasemonth"),
                PurchaseDay = reader.IsDBNull(reader.GetOrdinal("purchaseday")) ? (int?)null : reader.GetInt32("purchaseday"),
                GiftFrom = reader.IsDBNull(reader.GetOrdinal("giftfrom")) ? null : reader.GetString("giftfrom"),
                Quantity = reader.IsDBNull(reader.GetOrdinal("quantity")) ? (int?)null : reader.GetInt32("quantity"),
                ImageSrc = reader.IsDBNull(reader.GetOrdinal("imagesrc")) ? null : reader.GetString("imagesrc"),
                ReceiptSrc = reader.IsDBNull(reader.GetOrdinal("receiptsrc")) ? null : reader.GetString("receiptsrc")
            };

            return thing;
        }

        private static void AddParameters(MySqlCommand cmd, Thing thing)
        {
            cmd.Parameters.AddWithValue("@nameof", thing.NameOf);
            cmd.Parameters.AddWithValue("@purchaseprice", thing.PurchasePrice);
            cmd.Parameters.AddWithValue("@quantity", thing.Quantity);
            cmd.Parameters.AddWithValue("@description", thing.Description);
            cmd.Parameters.AddWithValue("@purchaseyear", thing.PurchaseYear);
            cmd.Parameters.AddWithValue("@purchasemonth", thing.PurchaseMonth);
            cmd.Parameters.AddWithValue("@purchaseday", thing.PurchaseDay);
            cmd.Parameters.AddWithValue("@imagesrc", thing.ImageSrc);
            cmd.Parameters.AddWithValue("@receiptsrc", thing.ReceiptSrc);
            cmd.Parameters.AddWithValue("@giftfrom", thing.GiftFrom);
        }
    }
}