﻿(function () {
    "use strict";
    var controllerId = "thingsController";
    angular.module("app").controller(controllerId, ["$scope", "$http", "$location", "$filter", "$rootScope", "googleService", thingsController]);

    function thingsController($scope, $http, $location, $filter, $rootScope, googleService) {
        $scope.things = [];
        $scope.goToThing = goToThing;
        $scope.searchTerm = "";

        $scope.sortBy = sortBy;
        $scope.propertyName = "nameOf";
        $scope.reverse = true;

        $scope.getDate = getDate;

        $scope.errorText = "";

        // $scope.userName = $rootScope.user;

        // $scope.signOut = googleService.handleAuthClick();

        getThings();

        function getThings() {
            $http({
                url: "/api/things",
                method: "GET"
            }).then(function (response) {
                $scope.things = response.data;
            }, function (response) {
                $scope.errorText = response.statusCode;
            });
        }

        function sortBy(propertyName) {
            $scope.reverse = ($scope.propertyName === propertyName) ? !$scope.reverse : false;
            $scope.propertyName = propertyName;
        }

        function goToThing(id) {
            $location.path("/things/" + id);
        }

        function getDate(thing) {
            if (thing.purchaseDate) {
                return $filter("date")(thing.purchaseDate, "yyyy, MMMM d");
            }

            if (thing.purchaseMonth) {
                return $filter("date")(new Date(thing.purchaseYear, thing.purchaseMonth), "yyyy, MMMM");
            }

            if (thing.purchaseYear) {
                return thing.purchaseYear;
            }

            return "";
        }
    }
})();