﻿(function () {
    "use strict";
    var controllerId = "thingController";
    angular.module("app").controller(controllerId, ["$scope", "$http", "$location", "$routeParams", thingController]);

    function thingController($scope, $http, $location, $routeParams) {
        var createNew = $routeParams.id === "new";
        $scope.goToThings = goToThings;
        $scope.save = save;
        $scope.errorText = "";

        initThing();
        createTitle();


        function initThing() {
            if (!createNew) {
                getThing($routeParams.id);
                return;
            }

            $scope.thing = {
                nameOf: null,
                description: null,
                purchasePrice: null,
                purchaseYear: null,
                purchaseMonth: null,
                purchaseDay: null,
                imageSrc: null,
                receiptSrc: null,
                giftFrom: null,
                tags: null,
                quantity: 1
            }
        }

        function goToThings() {
            $location.path("/things");
        }

        function createTitle() {
            if (createNew) {
                $scope.title = "Lägg till ny ägodel";
                return;
            }

            $scope.title = "Ändra på ägodel";
        }

        function getThing(id) {
            $http({
                url: "/api/things/" + id,
                method: "GET"
            }).then(function (response) {
                response.data.purchaseDate = new Date(response.data.purchaseDate);
                $scope.thing = response.data;
            }, function (response) {
                $scope.errorText = response.statusCode;
            });
        }

        function save() {
            if (createNew) {
                $http.post("/api/things/", $scope.thing, { header: { "Content-Type": "application/json" } })
                    .then(function (response) {
                        response.data.purchaseDate = new Date(response.data.purchaseDate);
                        $scope.thing = response.data;
                        goToThings();
                    },
                        function (response) {
                            $scope.errorText = response.statusCode;
                        });
            } else {
                $http.put("/api/things/" + $routeParams.id, $scope.thing, { header: { "Content-Type": "application/json" } })
                    .then(function (response) {
                        response.data.purchaseDate = new Date(response.data.purchaseDate);
                        $scope.thing = response.data;
                        goToThings();
                    }, function (response) {
                        $scope.errorText = response.statusCode;
                    });
            }
        }
    }
})();