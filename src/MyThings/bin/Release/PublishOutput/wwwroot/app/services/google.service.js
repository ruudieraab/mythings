﻿(function () {
    "use strict";

    var serviceId = "googleService";
    angular.module("app").factory(serviceId, ["$http", "$rootScope", googleService]);

    function googleService($http, $rootScope) {
        var gapi = window.gapi;
        var GoogleAuth; // Google Auth object.
        var scope = "https://www.googleapis.com/auth/drive";
        var clientId = "424903973850-q6aa44c73lv9e05vcju2t9duq0n9spvt.apps.googleusercontent.com";

        var service = {           
            initGapi: initGapi,
            handleAuthClick: handleAuthClick
        };

        return service;

        function initGapi() {
            // Load the API's client and auth2 modules.
            // Call the initClient function after the modules load.
            window.gapi.load('client:auth2', initClient);
        }

        function initClient() {
            gapi.client.init({
                'clientId': clientId,
                'scope': scope,
                'discoveryDocs': ['https://www.googleapis.com/discovery/v1/apis/drive/v3/rest']
            }).then(function () {
                GoogleAuth = gapi.auth2.getAuthInstance();

                // Listen for sign-in state changes.
                GoogleAuth.isSignedIn.listen(updateSigninStatus);

                // Handle initial sign-in state. (Determine if user is already signed in.)
                var user = GoogleAuth.currentUser.get();
                setSigninStatus();
            });
        }

        function handleAuthClick() {
            if (GoogleAuth.isSignedIn.get()) {
                // User is authorized and has clicked 'Sign out' button.
                GoogleAuth.signOut();
            } else {
                // User is not signed in. Start Google auth flow.
                GoogleAuth.signIn();
            }
        }

        function revokeAccess() {
            GoogleAuth.disconnect();
        }

        function setSigninStatus(isSignedIn) {
            var user = GoogleAuth.currentUser.get();
            var isAuthorized = user.hasGrantedScopes(scope);

            if (isAuthorized) {
                var profile = user.getBasicProfile();
                console.log(profile)
                $rootScope.user = profile.getEmail();
                console.log($rootScope.user)
                $rootScope.isAuthorized = true;
            } else {
                $rootScope.isAuthorized = false;;
            }
        }

        function updateSigninStatus(isSignedIn) {
            setSigninStatus();
        }
    }
})();