﻿(function () {
    "use strict";

    var app = angular.module("app", [
        "ngRoute"
    ]);

    //app.run(["googleService", function (googleService) {
    //    googleService.initGapi();
    //}]);

    app.config(["$routeProvider",
        function ($routeProvider) {
            $routeProvider
                .when("/", {
                    templateUrl: "app/login/login.html",
                    controller: "loginController"
                })
                .when("/things", {
                    templateUrl: "app/things/things.html",
                    controller: "thingsController"
                })
                .when("/things/:id", {
                    templateUrl: "app/things/thing.html",
                    controller: "thingController"
                })
        .otherwise({
            redirectTo: "/"
        });
        }]);
})();