﻿(function () {
    "use strict";
    var controllerId = "loginController";
    angular.module("app").controller(controllerId, ["$scope", "$http", "$location", "googleService", loginController]);

    function loginController($scope, $http, $location, googleService) {
        $scope.goToThings = function () {
            $location.path("/things");
        }
    }
})();