use prylregister_se_db;
drop table things;
drop table users;

use prylregister_se_db;
CREATE table users(
id char(40) NOT NULL, 
networkuserid NVARCHAR(250),
network NVARCHAR(100),
email NVARCHAR(250),
created DateTime NOT NULL DEFAULT CURRENT_TIMESTAMP,
modified DateTime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
PRIMARY KEY (id)
);

DELIMITER //
CREATE TRIGGER usersid BEFORE INSERT ON users
 FOR EACH ROW begin
 SET new.id = uuid();
end//
DELIMITER ;

use prylregister_se_db;
CREATE table things(
id char(40) NOT NULL, 
userid char(40),
purchaseprice int, 
purchaseyear int, 
purchasemonth int,
purchaseday int, 
quantity int NOT NULL, 
nameof NVARCHAR(100),
description NVARCHAR(500),
imagesrc NVARCHAR(250),
receiptsrc NVARCHAR(250),
giftfrom NVARCHAR(250),
created DateTime NOT NULL DEFAULT CURRENT_TIMESTAMP,
modified DateTime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
PRIMARY KEY (id),
FOREIGN KEY (userid) REFERENCES users(id)
);

DELIMITER //
CREATE TRIGGER thingsid BEFORE INSERT ON things
 FOR EACH ROW begin
 SET new.id = uuid();
end//
DELIMITER ;